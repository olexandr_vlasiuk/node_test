import { userListConstants } from '../../constants';

const initialState = {
  userList: [],
  allUsers: []
};

export const userListReducer = (state = initialState, action) => {
  switch (action.type) {
    case userListConstants.ADD_USERLIST:
      return {...state, userList: action.list};
    case userListConstants.SET_ALL_USERS:
      return {...state, allUsers: action.list};
    default:
      return state;
  }
};
