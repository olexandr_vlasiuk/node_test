import { messageConstants } from '../../constants';

const initialState = {
  chatHistory: []
};

export const messageReducer = (state = initialState, action) => {
  switch (action.type) {
    case messageConstants.ADD_HISTORY:
      return {chatHistory: action.chatHistory};
    case messageConstants.ADD_MESSAGE:
      let {chatHistory} = state;
      const updatedHistory = [
        ...chatHistory,
        action.payload
      ];
      return { chatHistory: updatedHistory};
    default:
      return state;
  }
};
