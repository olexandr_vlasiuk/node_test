import { userConstants } from '../../constants';

export const userActions = {
  login_success,
  login_failure,
  setUserStatistic
};

function login_success(id, token, role, name) {
  return {type: userConstants.USER_LOGIN_SUCCESS, auth: true, id, token, error: null, role, name};
}

function login_failure(error) {
  return {type: userConstants.USERS_LOGIN_FAILURE, auth: false, error, id: null, token: null, role: null};
}

function setUserStatistic(list) {
  return {type: userConstants.SET_USER_STATISTIC, list};
}
