import {all} from 'redux-saga/effects';
import {startStopChannel} from './task';

export default function* rootSaga() {
  yield all([startStopChannel()]);
}
