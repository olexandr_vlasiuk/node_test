import styled from 'styled-components';

export const Scrollable = styled.div `
  height: 100%;
  overflow: auto;
  width: 50%;
  margin: auto;
  background-color: gray;
  `;

export const NoDots = styled.div `
    hr {
      visibility: hidden;
    }
  `;
  
export const OutputText = styled.div `
    white-space: normal !important;
    word-break: break-all !important;
    overflow: initial !important;
    width: 100%;
    height: auto !important;
  `;
