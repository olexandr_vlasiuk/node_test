import React from 'react';
import {FlatButton, Paper, TextField, SelectField, MenuItem} from 'material-ui';
import {List, ListItem} from 'material-ui/List';
import Divider from 'material-ui/Divider';
import TabMenu from '../tabMenu/TabMenu';
import './chat.scss';
import {connect} from 'react-redux';
import { Scrollable, NoDots, OutputText } from './components/styledComponents';
import shortid from 'shortid';
import {join, leave, message, unregisterHandler, isAdminHandler} from '../../store/task';
import { messageActions } from '../../store/actions/index';

const ROLE_ADMIN = 1;

class Chat extends React.Component {
  constructor(props, context) {
    super(props, context)

    this.state = {
      input: '',
      room: null,
      prevRoom: null
    }
  }

  componentDidMount() {
    this.props.userReducer.role !== ROLE_ADMIN ?
      this.setState({room: this.props.userReducer.name}, () =>
      join(this.state.room, {role: this.props.userReducer.role, userName: this.props.userReducer.name}, (err, chatHistory) => {
        if (err)
          console.error(err);
        if(typeof chatHistory !== 'undefined')
          this.props.dispatch(messageActions.addChatHistory(chatHistory));
      })
    ) :
      '';
  }

  componentDidUpdate() {
    this.scrollChatToBottom();
  }

  componentWillUnmount() {
    unregisterHandler();
    leave(this.state.room, this.props.userReducer.name);
  }

  onInput = e => this.setState({input: e.target.value});

  scrollChatToBottom() {
    this.panel.scrollTo(0, this.panel.scrollHeight);
  }

  onSendMessage() {
    message({
      chatroomName: this.state.room,
      message: this.state.input,
      user: this.props.userReducer.name
    });
    this.setState({input: ''});
  }

  handleChange = (event, index, value) => {
    this.setState({prevRoom: this.state.room});
    this.setState({
      room: value
    }, () => {
      leave(this.state.prevRoom, this.props.userReducer.name);
      if (value != ' ')
        join(this.state.room, {role: this.props.userReducer.role, userName: this.props.userReducer.name}, (err, chatHistory) => {
          if (err)
            console.error(err);
          if(typeof chatHistory !== 'undefined')
            this.props.dispatch(messageActions.addChatHistory(chatHistory));
        });
    });
  }

  render() {
    return (<div className='chat'>
      <TabMenu {...this.props}/>
      <div className='workplace'>
        <Scrollable innerRef={panel => {
            this.panel = panel;
          }}>
          <List>
            {
              this.props.messageReducer.chatHistory.map(({
                user,
                message,
                event
              }, i) => [<NoDots key={'nodot' + i}>
                <ListItem className={user === this.props.userReducer.name
                    ? 'my-message'
                    : ''} key={shortid.generate()} primaryText={`${user} ${event || ''}`} secondaryText={message && <OutputText>
                    {message}
                  </OutputText>}/>
              </NoDots>
                ])
            }
          </List>
        </Scrollable>
        <div className='input-block'>
          {
            this.props.userReducer.role === ROLE_ADMIN && <SelectField
              floatingLabelText="Users"
              value={this.state.room}
              onChange={this.handleChange}
              className='users-select'>
                <MenuItem value=' ' primaryText=' '/>
                 {
                  this.props.userListReducer.userList.map(({
                    userName,
                    userEmail,
                    role
                  }, i) => {
                    if(role !== 1) {
                    return <MenuItem value={userName} key={shortid.generate()} primaryText={userName}/>
                  }
                  })
                }
              </SelectField>
          }
          <TextField hintText="Enter a message." floatingLabelText="Enter a message." multiLine={true} rows={4} rowsMax={4} onChange={this.onInput} value={this.state.input} onKeyPress={e => (
              e.key === 'Enter'
              ? this.onSendMessage()
              : null)}/>
        </div>
      </div>
    </div>);
  }
}

export default connect(state => state)(Chat);
