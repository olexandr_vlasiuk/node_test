import React from 'react';
import injectTapEventPlugin from 'react-tap-event-plugin';
import {FlatButton, Paper, TextField} from 'material-ui';
import Cookies from 'universal-cookie';
import {connect} from 'react-redux';
import './registration.scss';
import validator from 'pure-validator';
import { userActions } from '../../store/actions/index';
import {register, isAdminHandler} from '../../store/task';
import config from '../../../config/Configuration';

const ROLE_ADMIN = 1;

class Registration extends React.Component {
  state = {
    inputName: '',
    inputEmail: '',
    errorName: false,
    errorEmail: false
  };

  componentDidMount() {
    const cookies = new Cookies();
    let data = cookies.get('userRegData');
    data = (typeof data !== 'undefined')
      ? data
      : {
        userName: '',
        userEmail: ''
      };
    this.setState({inputName: data.userName, inputEmail: data.userEmail});
    if(this.props.userReducer.auth)
      this.props.history.push('/');
  }

  onInput = (e, state) => state === 'inputName'
    ? this.setState({inputName: e.target.value})
    : this.setState({inputEmail: e.target.value})

  onRegister = () => {
    const userName = this.state.inputName;
    const userEmail = this.state.inputEmail;
    if (this.checkData(userName, userEmail)) {
      const cookies = new Cookies();
      cookies.set('userRegData', {
        userName,
        userEmail,
        errorName: false,
        errorEmail: false
      }, {path: '/'});

      const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ name: userName, email: userEmail })
      };
      fetch(`${config.serverURL}api/auth/login`, requestOptions).then(data => data.json()).then((responseJson) => {
        if(responseJson.status === 200) {
          this.props.dispatch(userActions.login_success(responseJson.id, responseJson.token, responseJson.role, userName));
          register(userName, userEmail);
          isAdminHandler(responseJson.role === ROLE_ADMIN);
          this.props.history.push('/');
        }else{
          this.props.dispatch(userActions.login_failure(responseJson.error));
        }
      });
    }
  }

  checkData = (userName, userEmail) => {
    let isValid = true;
    this.setState({errorName: false, errorEmail: false});
    if (userName == '' || userName < 3) {
      this.setState({errorName: true});
      isValid &= false;
    }
    if (userEmail == '' || !validator.isEmail(userEmail)) {
      this.setState({errorEmail: true});
      isValid &= false;
    }
    return isValid;
  }

  render() {
    return (<Paper zDepth={2} className='register-form'>
      <TextField className='user-name' errorText={this.state.errorName
          ? 'Name field is reqiured'
          : ''} hintText='Your name' onChange={e => this.onInput(e, 'inputName')} value={this.state.inputName}/>
      <TextField className='user-email' errorText={this.state.errorEmail
          ? 'Email field is reqiured'
          : ''} hintText='Your email' onChange={e => this.onInput(e, 'inputEmail')} value={this.state.inputEmail}/>
      <FlatButton className='btn-submit' label='Submit' onClick={this.onRegister}/>
      {this.props.userReducer.error !== null && <p className='error'>{this.props.userReducer.error}</p>}
    </Paper>)
  }
}

export default connect(state => state)(Registration);
