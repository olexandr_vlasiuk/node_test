import React from 'react';
import {Link} from 'react-router-dom';
import {Tabs, Tab} from 'material-ui';
import {getUserList} from '../../store/task';

const ROLE_ADMIN = 1;
export default class TabMenu extends React.Component {
  state = {
    value: 0
  };

  componentDidMount() {
    if(!this.props.userReducer.auth)
      this.props.history.push('/login');
    getUserList();
    switch (this.props.location.pathname) {
      case '/':
        this.setState({value: 0})
        break;
      case '/chat':
        this.setState({value: 1})
        break;
      case '/commands':
        this.setState({value: 3})
        break;
      case '/test-statistics':
        this.setState({value:4})
        break;
      case '/users':
        this.setState({value:5})
        break;
      default:
        this.setState({value: 1})
        break;
    }
  }

  render() {
    return (<Tabs value={this.state.value}>
      <Tab
        value={0}
        label="Home"
        containerElement={<Link to="/" />}/>
      <Tab
        value={1}
        label="Chat"
        containerElement={<Link to="/chat" />}/>
      {this.props.userReducer.role === ROLE_ADMIN && <Tab
        value={3}
        label="Commands"
        containerElement={<Link to="/commands" />}/>}
      {this.props.userReducer.role !== ROLE_ADMIN && <Tab
        value={4}
        label="Test statistics"
        containerElement={<Link to="/test-statistics" />}/>}
      {this.props.userReducer.role === ROLE_ADMIN && <Tab
        value={5}
        label="All users"
        containerElement={<Link to="/users" />}/>}
    </Tabs>)
  }
}
