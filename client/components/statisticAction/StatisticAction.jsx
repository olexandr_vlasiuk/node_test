import React from 'react';
import {FlatButton, Paper, TextField, SelectField, MenuItem} from 'material-ui';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import Checkbox from 'material-ui/Checkbox';
import TabMenu from '../tabMenu/TabMenu';
import './statisticAction.scss';
import {connect} from 'react-redux';
import config from '../../../config/Configuration';

class StatisticsAction extends React.Component {
  state = {
    value: 'one'
  };

  handleChange = (event, index, value) => {
    this.setState({value})
    this.onSendAction({action: 'select', value})
  };

  handleChangeRadio = (event, value) =>
    this.onSendAction({action: 'change', value});

  onSendAction = action => {
    const requestOptions = {
          method: 'POST',
          headers: { 'Content-Type': 'application/json', 'x-access-token': this.props.userReducer.token },
          body: JSON.stringify({ user: this.props.userReducer.name, action: action })
    };
    fetch(`${config.serverURL}api/statistic`, requestOptions);
  }

  render() {
    return (<div className='statistics-action'>
      <TabMenu {...this.props}/>
      <div className='actions'>
        <div className='block'>
          <FlatButton label="Default" onClick={()=> this.onSendAction({action: 'click', value:'Default'})}/>
          <FlatButton label="Primary" primary={true} onClick={()=> this.onSendAction({action: 'click', value:'Primary'})}/>
          <FlatButton label="Secondary" secondary={true} onClick={()=> this.onSendAction({action: 'click', value:'Secondary'})}/>
        </div>
        <div className='block'>
          <SelectField floatingLabelText="Select Field" value={this.state.value} onChange={this.handleChange}>
            <MenuItem value='one' primaryText="One"/>
            <MenuItem value='two' primaryText="Two"/>
            <MenuItem value='three' primaryText="Three"/>
            <MenuItem value='four' primaryText="Four"/>
            <MenuItem value='five' primaryText="Five"/>
          </SelectField>
        </div>
        <div className='block'>
          <RadioButtonGroup name="shipSpeed" defaultSelected="not_light" onChange={this.handleChangeRadio}>
            <RadioButton value="light" label="Simple"/>
            <RadioButton value="not_light" label="Selected by default"/>
          </RadioButtonGroup>
        </div>
        <div className='block'>
          <Checkbox label="Checkbox 1" onCheck={(e,isChecked) => this.onSendAction({action: 'check', value: 'checkbox_1'})} />
          <Checkbox label="Checkbox 2" onCheck={(e,isChecked) => this.onSendAction({action: 'check', value: 'checkbox_2'})} />
        </div>
      </div>
    </div>);
  }
}

export default connect(state => state)(StatisticsAction)
