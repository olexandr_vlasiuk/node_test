var mongoose = require('mongoose');
var db = mongoose.connect(`mongodb://localhost:27017/node-test`, { useMongoClient: true });

var User = require('../../components/user/User');
var Action = require('../../components/statistic/Action');
var Message = require('../../components/chat/Message');
var Command = require('../../components/chat/Command');

Message.remove({})
.then(() => {
  Action.remove({}).then(() => {
    Command.remove({}).then(() => {
      User.remove({}).then(() => {
        mongoose.disconnect();
      });
    });
  });
});



// var user = new User({
//   _id: new mongoose.Types.ObjectId(),
//   name: "Name",
//   email: "name@name.name",
//   role: "2",
//   register: "2018-05-23 16:35:48.463",
//   lastvisit: "2018-06-04 12:51:11.893",
//   lastaction: "2018-05-31 18:58:22.513"
// })
// User.create({
//   _id: new mongoose.Types.ObjectId(),
//   name: "Name",
//   email: "name@name.name",
//   role: "2"
  // register: "2018-05-23 16:35:48.463",
  // lastvisit: "2018-06-04 12:51:11.893",
  // lastaction: "2018-05-31 18:58:22.513"
// });

// Action.find({}).remove().exec();
// Message.find({}).remove().exec();
// Command.find({}).remove().exec();
// User.find({}).remove().exec();

//
