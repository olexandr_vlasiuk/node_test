var mongoose = require('mongoose');
var config = require('../../config/Configuration');
mongoose.connect(`${config.db.host}${config.db.database}`, { useMongoClient: true });
