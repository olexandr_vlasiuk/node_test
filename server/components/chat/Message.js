var mongoose = require('mongoose');
var config = require('../../../config/Configuration');
const messageSchema = {
  _id: mongoose.Schema.Types.ObjectId,
  sender: {
    type: mongoose.Schema.Types.ObjectId,
    ref: config.db.userTable
  },
  reciever: {
    type: mongoose.Schema.Types.ObjectId,
    ref: config.db.userTable
  },
  message: String,
  date: {
    type: Date,
    default: Date.now
  }
}
mongoose.model(config.db.messageTable, messageSchema);

module.exports = mongoose.model(config.db.messageTable);
