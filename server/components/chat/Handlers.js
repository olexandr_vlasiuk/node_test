const User = require('../user/User');
const Message = require('./Message');
const Command = require('./Command');
const moment = require('moment');
const mongoose = require('mongoose');
const ROLE_ADMIN = 1;

function makeHandleEvent(client, clientManager, chatroomManager) {
  function ensureExists(getter, rejectionMessage) {
    return new Promise(function(resolve, reject) {
      const res = getter()
      return res
        ? resolve(res)
        : reject(rejectionMessage)
    })
  }

  function ensureUserSelected(clientId) {
    return ensureExists(() => clientManager.getUserByClientId(clientId), 'select user first')
  }

  function ensureValidChatroom(chatroomName) {
    return ensureExists(() => chatroomManager.getChatroomByName(chatroomName), `invalid chatroom name: ${chatroomName}`)
  }

  function ensureValidChatroomAndUserSelected(chatroomName) {
    return Promise.all([
      ensureValidChatroom(chatroomName),
      ensureUserSelected(client.id)
    ]).then(([chatroom, user]) => Promise.resolve({chatroom, user}))
  }

  function handleEvent(chatroomName, createEntry) {
    return ensureValidChatroomAndUserSelected(chatroomName).then(function({chatroom, user}) {
      // append event to chat history
      let entry1 = createEntry();
      console.log(entry1);
      const entry = {
        user,
        event: entry1.event,
        message: entry1.message
      }
      chatroom[1].addEntry(entry)
      // notify other clients in chatroom
      chatroom[1].broadcastMessage({
        chat: chatroomName,
        user: user,
        event: entry1.event,
        message: entry1.message
      })
      return chatroom
    })
  }

  return handleEvent
}

module.exports = function(client, clientManager, chatroomManager) {
  const handleEvent = makeHandleEvent(client, clientManager, chatroomManager)

  function handleRegister(userName, userEmail, callback) {
    sendUserList();
    User.findOne({ name: userName, email: userEmail }).then(user => {
      if (typeof clientManager.getUserByName(userName).user !== 'undefined') {
        clientManager.removeUserByName(userName)
      }
      clientManager.registerClient(client, {userName, userEmail, role: user.role});
      callback(user);
    });
  }

  function sendUserList() {
    let users = clientManager.getAllUsers();
    let admins = clientManager.getAdminUser();
    admins.forEach((u, k) => {
      u.emit('userlist', users)
    });
  }

  function handleJoin(chatroomName, user, callback) {
    let chatName = '';
    if (user.role === ROLE_ADMIN) {
      chatName = chatroomName;
      chatroomManager.addRoom(user.userName);
    } else {
      chatName = user.userName;
      chatroomManager.addRoom(chatroomName);
    }
    const createEntry = () => ({event: `joined ${chatroomName}`})

    handleEvent(chatroomName, createEntry).then(function(chatroom) {
      // add member to chatroom
      chatroom[1].addUser(client);
      // send chat history to client
      callback(null, chatroom[1].getChatHistory());
    }).catch(callback);
  }

  function handleLeave(chatroomName, name, callback) {
    sendUserList();
    const createEntry = () => ({event: `left ${chatroomName}`})

    handleEvent(chatroomName, createEntry).then(function(chatroom) {
      // remove member from chatroom
      chatroom[1].removeUser(client.id);
      clientManager.removeUserByName(name);
      sendUserList();
      callback(null);
    }).catch(callback);
  }

  function updateLastAction(id) {
    User.findByIdAndUpdate(id, {$set:{lastaction: moment()}}, function(err, result){
      if(err)
          console.log(err);
      console.log("RESULT: " + result);
    });
  }

  function saveMessage(sender, reciever, message) {
    User.findOne({'name': sender}).then((userSender) => User.findOne({'name': reciever}).then((userReciever) => {
      Message.create({
        _id: new mongoose.Types.ObjectId(),
        sender: userSender._id,
        reciever: userReciever._id,
        message: message
      });
      updateLastAction(userSender._id);
    }));
  }

  function handleMessage({chatroomName, message, user} = {}, callback) {
    if (user === chatroomName) {
      saveMessage(user, 'Admin', message);
    } else {
      saveMessage(user, chatroomName, message);
    }
    const createEntry = () => ({message});
    handleEvent(chatroomName, createEntry);
  }

  function handleCommand({chatroomName, cmd} = {}, callback) {
      User.findOne({'name': chatroomName}).then((user)  => {
        Command.create({
            _id: new mongoose.Types.ObjectId(),
            reciever: user._id,
            command: cmd
        });
        User.findOne({'name': 'Admin'}).then((admin) => {
          updateLastAction(admin._id);
        });
      });
    const room = chatroomManager.getChatroomByName(chatroomName);
    room[1].broadcastCommand(cmd);
    callback(null);
  }

  return {
    handleRegister,
    handleJoin,
    handleLeave,
    handleMessage,
    sendUserList,
    handleCommand
  }
}
