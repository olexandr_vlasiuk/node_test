var mongoose = require('mongoose');
var config = require('../../../config/Configuration');

var UserSchema = new mongoose.Schema({
  name: String,
  email: String,
  role: Number,
  register: {
    type: Date,
    default: Date.now
  },
  lastvisit: {
    type: Date,
    default: Date.now
  },
  lastaction: {
    type: Date,
    default: Date.now
  }
});

mongoose.model(config.db.userTable, UserSchema);

module.exports = mongoose.model(config.db.userTable);
