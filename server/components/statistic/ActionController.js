var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var VerifyToken = require('../VerifyToken');
var moment = require('moment');
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

var Action = require('./Action');
var User = require('../user/User');

router.post('/', VerifyToken, function(req, res) {
  User.findOne({'name': req.body.user}).then(user => {
    Action.create({
      _id: new mongoose.Types.ObjectId(),
      user: user._id,
      action: JSON.stringify(req.body.action)
    }, function(err, action) {
      if (err)
        return res.status(500).send({status: 500, error: "There was a problem adding the information to the database.", err: err});
      updateLastAction(user._id);
      return res.status(200).send({status: 200});
    });
  });
});

function updateLastAction(id) {
  User.findByIdAndUpdate(id, {$set:{lastaction: moment()}}, function(err, result){
    if(err)
        console.log(err);
    console.log("RESULT: " + result);
  });
}

module.exports = router;
