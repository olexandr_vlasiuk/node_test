var mongoose = require('mongoose');
var config = require('../../../config/Configuration');

const actionSchema = {
  _id: mongoose.Schema.Types.ObjectId,
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: config.db.userTable
  },
  action: String,
  date: {
    type: Date,
    default: Date.now
  }
}

mongoose.model(config.db.actionTable, actionSchema);

module.exports = mongoose.model(config.db.actionTable);
