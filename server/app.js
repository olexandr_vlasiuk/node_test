var express = require('express');
var app = express();
var db = require('./db');
var cors = require('cors');
var config = require('../config/Configuration');

app.use(cors({
  origin: config.clientURL,
  credentials: true
}));

var UserController = require('./components/user/UserController');
app.use('/api/users', UserController);

var ActionController = require('./components/statistic/ActionController');
app.use('/api/statistic', ActionController);

var AuthController = require('./components/auth/AuthController');
app.use('/api/auth', AuthController);

module.exports = app;
